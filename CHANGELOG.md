# Changelog

## v2.2

### Additions

* `\pnorm`, `\ppnorm`, `\qnorm`, `\qqnorm`, `\2norm`, `\s2norm` shortcuts 
* `\argmin` and `\argmax`
* add `axiom` environment to thms-gmpcolor
* add optional argument for function to `derivquotient`
* differential operators for total (`der`) and partial (`pder`) differentials
* add options for total derivatives (`toder`)
* add options for vector calculus operators div, grad, rot
* add `skadot` as symbol for eucliedean scalar product (can be renewed)
* add some abbreviations (bzgl, ig/iA, etc/usw, cf/vgl)

### Fixes

* Correct colours and environment names in docu for thms-gmpcolor
* fix behaviour of mixed partial differentials
* fix language assocation of abbreviation Wrt/Bzgl 

## v2.1

### Additions

* Allow empty input or `none` for `theorem numbering` and `number within` keys to support theorems with simple numbers
* Banach dual space: `\dual`, `\DS` or `\DR`
* `qed symbol` option for `\newthm`
* new style 'gmp-color' for theorems: a colorful version of gmp

### Changes

* Simplify `misitex-blank-thm` to `misitex-trivlist` (except for beamer, which needs special treatment)
* Internal naming of theorem counters
* `color` argument defaults to `black` instead of `gray` in `\newthm`
* Make lorenz style actually use `color` argument
* Change the color of remarks and examples for the gmp-styles to gray

### Fixes

* use `minipage` in theorem style lorenz to allow for `\hfill`
* Fix Specing issue of `\inv` and friends with operatornames (#12)

## v2.0

### Fixes

* Minor fix in the internals of theorems

### Additions

* Support for the `beamer` class: 
  * Handle theorem counters on overlays correctly
  * Adjust the `misitex-blank-thm` environment
  * rename the `note` environment in the predefined theorems
  * Make boxes for proofs an option
  * Make boxes for theorems on option
  
### Changes

* Make `\loadfile` look for files in subdirectories first. Fixes issues, when version in subdirectory is older than the one in the `TeX` path.

## v1.5

### Fixes

* Characteristic defaults to `\mathds{1}` rather than `\mathbb{1}`
* Put optional arguments in `{}` to allow commands with optional parameters as (optional) parameters
* Typo in theorems set `hirsch`: english plural for `theorem` environment
* Fix indention in theorems ([#8](https://gitlab.com/HirschHeissIch/misitex/issues/8))
* Support `marvosym` symbols in mathmode natively (e.g. type `$\Lightning$` instead of `$\text{\Lightning}$`)
* Fix vertical space issue in equations introduced by `misitex.cleveref-write`
* On some machines, the `number within` option for theorems didn't work due to a problem with
  the `chngcntr` package and `\begingroup`/`\endgroup`

### Additions

* `before head` option for theorems
* `\powerset`
* Make `\qedsymbol` an option
* Add option for automatically including `\proofname` in the optional argument of the `proof` environment
* Make font style of `\proofname` customizable
* Option `final`
* Option `hide proofs`

## v1.4

### Fixes

* Subscript behaviour for `\newenclose` and `\newcenclose`

### Changes

* Backend for `\newenclose` and `\newcenclose`. **Breaks backward compatibility** for the optional size argument.
* Make MiSiTeX loadable from subdirectories

### Additions

* `\loadfile` command to allow MiSiTeX to be loaded from subdirectories

## v1.3

### Fixes

* Reconstruction of bracketing environments to allow more cusotmizable spacing options
* Missing star and wrong position in `\w(s)too`
* Footnote behaviour for theorems *not* based on `tcolorbox`

### Changes

* Make argument of `\lspan` optional. **Breaks backward compatibility!**

### Additions

* Complex unit `\i`
* `\stackarrow`, `\convarrow`, `\convharpoonup`, `\convharpoondown` as new base for convergence commands
* `\slim`, `\wlim`, `wslim`
* `\eigs`: `\eig` with optional operator
* Option `eigenspace style`: `subscript` or `braces`
* Spectral Theory. See documentation

## v1.2

### Fixes

* Fix default setting of `punctation` key for `\newthm`
* Refine numbering in `thms-hirsch` and `thms-gmp`
* Rename `punctation` to `punctuation` b/c author can't english …
* Typo in `\obda`

### Additions

* `\commath` and `\kommath` for punctuation marks after equations in text mode with space
* `\compos` (`\after`, `\nach`) for compositions of functions
* `\orthcomp` and `\ocomp` for orthogonal complements
* `\orthogonal` and `\orth` for orthogonality as relation
* `\cconj` and `\conj` for complex/conjex conjugation
* `\adj` for adjungation
* `\tinf` and `\tzero` as convergence shortcuts
* `\wtoo` and `\wstoo`, `\w(s)tinf` and `\w(s)tzero`
* Make default position for `\too` & friends an option
* Option for weak (star) convergence style: harpoon vs. `\xrightarrow{w(^*)}`

## v1.1

* Major reconstruction for theorem handling
* Fix `\sca`: Use `\enclose` instead of `\cenclose` to avoid unnecessary dot for empty size option
* Use upright `T` for `\transp`
* Add `cwl` file for TeXStudio
* Rename `\to` to `\too` due to problems with e.g. `\to[0,1]` and add `\wtoo`
* Add `\Linf` as shortcut for `\L{\infty}`
* Readjustment in how the `all` key is set