# MiSiTeX

A custom LaTeX Package providing commands and environments for writing math in LaTeX hassle free.

## Installation

Download the repository (either clone or use the latest [release](https://gitlab.com/HirschHeissIch/misitex/releases)) and

 * put in somewhere in your TeX-Path or
 * dump all the `.sty` files in a subdirectory. In this case you *must* load the `currfile` package before loading MiSiTeX. For details, have a look at the [documentation](https://gitlab.com/HirschHeissIch/misitex/blob/master/pdf/misitex_documentation.pdf).

**TeXStudio:** MiSiTeX comes with a `cwl` file for autocompletion. Place it in your [path](http://texstudio.sourceforge.net/manual/current/usermanual_en.html#CWLDESCRIPTION).

## Contributing

Contributions are welcome!

If you enjoy using MiSiTeX, but miss commands for a specific area of mathematics, please consider writing a file `my_area.sty` that may be loaded my MiSiTeX and document the commands in `misitex_documentation.tex`. Similarly, new theorem styles may be incorporated.

If you have difficulties getting started with Git(Lab) and/or the internals of MiSiTeX, feel free to contact [Hinrich Mahler](gitlab.com/HirschHeissIch).