\MiSiTeX#*
\misitex#*

# Theorem definition
\newthm{name}{options}#N
\newaliasthm{name}{alias}{sing}{plur}{en sing}{en plur}#N

# Thms-Short
\begin{cor}#\env
\begin{defnsatz}#\env
\begin{expl}#\env
\begin{note}#\env
\begin{satz}#\env
\end{cor}#\env
\end{defnsatz}#\env
\end{expl}#\env
\end{note}#\env
\end{satz}#\env

# Thms-Long
\begin{beispiel}#\env
\begin{bemerkung}#\env
\begin{corollary}#\env
\begin{definitionsatz}#\env
\begin{definition}#\env
\begin{example}#\env
\begin{korollar}#\env
\begin{lemma}#\env
\begin{note}#\env
\begin{proposition}#\env
\begin{satz}#\env
\begin{theorem}#\env
\end{beispiel}#\env
\end{bemerkung}#\env
\end{corollary}#\env
\end{definitionsatz}#\env
\end{definition}#\env
\end{example}#\env
\end{korollar}#\env
\end{lemma}#\env
\end{note}#\env
\end{proposition}#\env
\end{satz}#\env
\end{theorem}#\env

# Typography
\bzw#n
\Bzw#n
\dh#n
\Dh#n
\fa#n
\Fa#n
\fue#n
\Fue#n
\oBdA#n
\OBdA#n
\obda#n
\Obda#n
\zB#n
\ZB#n
\ac
\Ac#n
\ae#n
\Ae#n
\eg#n
\Eg#n
\ie#n
\Ie#n
\lsc#n
\Lsc#n
\st#n
\St#n
\usc#n
\Usc#n
\wrt#n
\Wrt#n
\wloss#n
\Wloss#n
\wolog#n
\Wolog#n

# Bracketing
\abs[opt]{arg}#m
\abs{arg1}#m
\norm[opt]{arg}#m
\norm{arg}#m
\norm{arg}[idx]#m
\pnorm[opt]{arg}#m
\pnorm{arg}#m
\pnorm{arg}[idx]#m
\ppnorm[opt]{arg}#m
\ppnorm{arg}#m
\ppnorm{arg}[idx]#m
\qnorm[opt]{arg}#m
\qnorm{arg}#m
\qnorm{arg}[idx]#m
\qqnorm[opt]{arg}#m
\qqnorm{arg}#m
\qqnorm{arg}[idx]#m
\2norm[opt]{arg}#m
\2norm{arg}#m
\2norm{arg}[idx]#m
\s2norm[opt]{arg}#m
\s2norm{arg}#m
\s2norm{arg}[idx]#m
\of[opt]{arg}#m
\of{arg}#m
\set[opt]{arg1}{arg2}#m
\set{arg1}{arg2}#m
\sset[opt]{arg}#m
\sset{arg}#m
\sca[opt]{arg1}{arg2}#m
\sca{arg1}{arg2}#m
\sca{arg1}{arg2}[idx]#m
\ska[opt]{arg1}{arg2}#m
\ska{arg1}{arg2}#m
\ska{arg1}{arg2}[idx]#m

# Math Typesetting
\blank#m
\dummy#m
\const#m
\commath#m
\commath[del]#m
\kommath#m
\kommath[del]#m
\op{operator}#m
\contradiction#m
\field{arg}#m*
\C#m
\R#m
\N#m
\Q#m
\Z#m
\Re#m
\Im#m
\e#m
\i#m
\id#m
\transp#m
\transp[arg]#m
\orthcomp#m
\orthcomp[arg]#m
\ocomp#m
\ocomp[arg]#m
\orthogonal#m
\orth#m
\inv#m
\inv[arg]#m
\adj#m
\adj[arg]#m
\conj#m
\conj[arg]#m
\conj{arg}#m
\cconj{arg}#
\sign#m
\ball{r}{x}#m
\close{arg}#m
\comp#m
\comp[arg]#m
\compos#m
\nach#m
\after#m
\interior#m
\interior[arg]#m
\open#m
\open[arg]#m
\modulo{arg1}{arg2}#m
\equation#m
\equation[nmb]#m
\equation[sing][plur]#m
\equation[nmb][sing][plur]#m
\Space{Symb}#m
\upSpace{Symb}#m
\dsum#m
\dplus#m
\dtimes#m
\dprod#m
\seq{symb}{idx}#m
\seq{symb}{idx}[set]#m
\seq[opt]{symb}{idx}#m
\seq[opt]{symb}{idx}[set]#m
\subseq{symb}{idx1}{idx2}#m
\subseq{symb}{idx1}{idx2}[set]#m
\subseq[opt]{symb}{idx1}{idx2}#m
\subseq[opt]{symb}{idx1}{idx2}[set]#m
\lspan#m
\lspan[set]#m
\clospan#m
\clospan[set]#m
\Span#m
\Span[set]#m
\cloSpan#m
\cloSpan[set]#m
\Graph#m

# Convergence
\stackarrow{arrow}{top}{btm}#m
\stackarrow{arrow}{top}[tspace]{btm}[bspace]#m
\convarrow{top}{btm}#m
\convarrow{top}[tspace]{btm}[bspace]#m
\convharpoonup{top}{btm}#m
\convharpoonup{top}[tspace]{btm}[bspace]#m
\convharpoondown{top}{btm}#m
\convharpoondown{top}[tspace]{btm}[bspace]#m
\too#m
\too[txt]#m
\too[txt][pos]#m
\wto#m
\wtoo#m
\wtoo[txt]#m
\wtoo[txt][pos]#m
\wsto#m
\wstoo#m
\wstoo[txt]#m
\wstoo[txt][pos]#m
\tinf#m
\tinf[var]#m
\tinf[var][pos]#m
\wtinf#m
\wtinf[var]#m
\wtinf[var][pos]#m
\wstinf#m
\wstinf[var]#m
\wstinf[var][pos]#m
\tzero#m
\tzero[var]#m
\tzero[var][pos]#m
\wtzero#m
\wtzero[var]#m
\wtzero[var][pos]#m
\wstzero#m
\wstzero[var]#m
\wstzero[var][pos]#m
\slim#m
\wlim#m
\wslim#m

\span{arg}#m
\rest#m
\restr{fct}{set}#m
\Range#m
\Rang#m
\Ran#m
\Rg#m
\range#m
\rang#m
\ran#m
\rg#m
\bild#m
\Bild#m
\image#m
\Image#m
\Kern#m
\Ker#m
\kernel#m
\NS#m
\ind#m
\charac#m
\dom#m
\Dom#m
\pos{fct}#m
\negprt{fct}#m
\argmin#m
\argmax#m
\spt#m
\Spt#m
\supp#m
\fourier{fct}#m
\invfourier{fct}#m
\conv#m
\embed#m

\HR{symb}#m
\HS{symb}#m
\H#m
\BR{symb}#m
\BS{symb}#m
\dual{space}#m
\DS{space}#m
\DR{space}#m
\L{exp}#m
\Lloc{exp}#m
\Lp#m
\Lq#m
\Lploc#m
\Lqloc#m
\Linf#m
\SS#m
\schwartz#m
\CC#m
\CCc#m
\test#m
\DD#m
\QS{arg1}{arg2}#m
\QR{arg1}{arg2}#m
\lin#m
\lin[qual]#m
\sobolev{order}#m
\sobolev{order}[exp]#m
\sobolev{order}[exp]<text>#m
\sobolev*{order}#m
\sobolev*{order}[exp]#m
\sobolev*{order}[exp]<text>#m

# Derivatives
\div#m
\rot#m
\grad#m
\gradient#m
\laplace#m
\subdiff#m
\sdiff#m
\derivsep#m*
\dd{fct}{var}#m
\dd[idx]{fct}{var}#m
\d{var}#m
\d[exp]{var}#m
\dq{var}#m
\dq[exp]{var}#m
\sdq{var}#m
\pard{var}#m
\pard[exp]{var}#m
\pardq{var}#m
\pardq[exp]{var}#m
\parsdq{var}#m
\dxy#m
\dxyz#m
\dqxy#m
\dqxyz#m
\sdqxy#m
\sqyxyz#m
\pardxy#m
\pardxyz#m
\pardqxy#m
\pardqxyz#m
\spardxy#m
\spardxyz#m

# Measure Theory
\leb#m
\dleb#m
\nac#m
\acl#m
\nacl#m
\radon#m
\prob#m

# Linear Algebra
\diag#m
\spur#m
\trace#m
\eig{op}{ev}#m
\eigs[op]{ev}#m
\eigs{ev}#1

# Orlicz Spaces
\Llux#m
\Llux[opt]#m
\Lorl#m
\Lorl[opt]#m
\luxnorm{arg}#m
\luxnorm[opt]{arg}#m
\orlnorm{arg}#m
\orlnorm[opt]{arg}#m
\Lexp#m
\LlogL#m

# Spactral Theory
\spec#m
\spec[part]#m
\res#m
\reso{num}#m
\reso[op]{num}#m
\reso[op][size]{num}#m
\nrstoo[desc][pos]#m
\srstoo[desc][pos]#m
\wrstoo[desc][pos]#m
\ntoo[desc][pos]#m
\formtoo[desc]{form}#m
\formtoo[desc][pos]{form}#m
\eigop{num}#m
\eigop[op]{num}#m
\eigop[op][size]{num}#m
\deig{num}{vec}#m
\deig[op]{num}{vec}#m
\form{name}#m
\opform{operator}#m
\fdom{operator}#m


