%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Math. Typesetting		%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Make MarvoSym symbols usable in math mode
\renewcommand{\mvchr}[1]{\mbox{\mvs\symbol{#1}}}

%% Wrapper for \inv and friends to behave well with math operators
\newcommand{\misitex@mathspacing}[1]{
	\mathopen{}\mathclose{#1}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% General				%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Dummies
\newcommand{\blank}{\, \boldsymbol{.}\, }
\newcommand{\dummy}{\blank}
\DeclareMathOperator{\const}{const}

%% Comma after equation
\newcommand{\commath}[1][,]{\,\text{#1}}
\newcommand{\kommath}[1][,]{\commath[{#1}]}

%% Operatorname
\newcommand{\op}[1]{\operatorname{#1}}

%% Contradiction
\newcommand{\contradiction}{\Lightning}

%% Fields
\newrobustcmd{\field}[1]{\mathbb{#1}}
\newrobustcmd{\C}{\field{C}}
\newrobustcmd{\R}{\field{R}}
\newrobustcmd{\N}{\field{N}}
\newrobustcmd{\Q}{\field{Q}}
\newrobustcmd{\Z}{\field{Z}}

%% Imaginary and Real part
\let\obsoleteRe\Re
\let\obsoleteIm\Im
\renewrobustcmd{\Re}{\operatorname{Re}}
\renewrobustcmd{\Im}{\operatorname{Im}}

%% Exponents
\newrobustcmd{\inv}[1][]{\misitex@mathspacing{{#1}^{-1}}}
\newrobustcmd{\transp}[1][]{\misitex@mathspacing{{#1}^{\mathsf{T}}}}
\newrobustcmd{\orthcomp}[1][]{\misitex@mathspacing{{#1}^{\bot}}}
\newrobustcmd{\ocomp}[1][]{\orthcomp[{#1}]}
\newrobustcmd{\comp}[1][]{\misitex@mathspacing{{#1}^{\!\complement}}}
\ifthenelse{\equal{\misitex@conjstyle}{ast}}{%
		\newrobustcmd{\conj}[1][]{\misitex@mathspacing{{#1}^{*}}}
	}{\ifthenelse{\equal{\misitex@conjstyle}{overline}}{
		\newrobustcmd{\conj}[1]{\overline{#1}}
	}{}
}
\IfStrEqCase{\misitex@adjstyle}{%
	{ast}{\newcommand{\misitex@adjarg}{*}}
	{H}{\newcommand{\misitex@adjarg}{\mathsf{H}}}
}
\newrobustcmd{\adj}[1][]{\misitex@mathspacing{{#1}^{\misitex@adjarg}}}
\newrobustcmd{\hadj}[1][]{\misitex@mathspacing{{#1}^{\mathsf{H}}}}

%% Different Shortcuts
\newrobustcmd{\e}{\ensuremath{\mathrm{e}}}
\let\obsoletei\i
\renewcommand{\i}{\ensuremath{\mathrm{i}}}
\DeclareMathOperator{\id}{id}
\newrobustcmd{\orthogonal}{\perp}
\newrobustcmd{\orth}{\orthogonal}
\DeclareMathOperator{\sign}{sign}
\newrobustcmd{\ball}[2]{\upSpace{B}_{#1}\of{#2}}
\newrobustcmd{\close}[1]{\overline{#1}}
\newrobustcmd{\compos}{\circ}
\newrobustcmd{\nach}{\circ}
\newrobustcmd{\after}{\circ}
\newrobustcmd{\cconj}[1]{\overline{#1}}

%% Interior of set
\ifthenelse{\equal{\misitex@interiorstyle}{accent}}
{
	\newrobustcmd{\interior}[1][]{
		\ifstrempty{#1}
			{^{\circ}}
			{\mathring{#1}}
	}
}{
	\ifthenelse{\equal{\misitex@interiorstyle}{exponent}}
	{
		\newrobustcmd{\interior}[1][]{\misitex@mathspacing{{#1}^{\circ}}}
	}{}
}
\newrobustcmd{\open}[1][]{\interior[{#1}]}

%% Modulo
\DeclareMathOperator{\misitex@modulo}{mod}
\newcommand{\modulo}[2]{%
	\misitex@modulo\of{#1,#2}%
}

%% Equation redefinition for custom naming and stuff
\loadfile{equation}

%% Spaces
\newrobustcmd{\Space}[1]{\ensuremath{\mathcal{#1}}}
\newrobustcmd{\upSpace}[1]{\ensuremath{\mathrm{#1}}}

%% Direct …
\newrobustcmd{\dsum}{\oplus}
\newrobustcmd{\dplus}{\oplus}
\newrobustcmd{\dtimes}{\otimes}
\newrobustcmd{\dprod}{\otimes}

%% Bracketing envs
\loadfile{bracketing}

%% Sequences
\newenclose{\@seq}()
\NewDocumentCommand{\seq}{o m m O{}}{
	\IfValueTF{#1}{%
		\ifstrempty{#4}{
			\@seq[{#1}]{{#2}_{#3}}
		}{
			\@seq[{#1}]{{#2}_{#3}}_{#3\in#4}
		}
	}{%
		\ifstrempty{#4}{
			\@seq{{#2}_{#3}}
		}{
			\@seq{{#2}_{#3}}_{#3\in#4}
		}
	}
}
\NewDocumentCommand{\subseq}{o m m m O{}}{
	\IfValueTF{#1}{%
		\ifstrempty{#5}{
			\@seq[{#1}]{%
				{#2}_{{#3}_{#4}}%
			}
		}{
			\@seq[{#1}]{{#2}_{{#3}_{#4}}}_{#4\in#5}
		}
	}{%
		\ifstrempty{#5}{
			\@seq{%
				{#2}_{{#3}_{#4}}%
			}
		}{
			\@seq{{#2}_{{#3}_{#4}}}_{#4\in#5}
		}
	}
}

%% Cenetered Colon
\mathtoolsset{centercolon}

%% Linear span
\DeclareMathOperator{\misitex@span}{span}
\DeclareMathOperator{\misitex@Span}{Span}
\newcommand{\lspan}[1][]{
	\ifempty{#1}
		\misitex@span
	\else
		\misitex@span\sset{#1}
	\fi
}
\newcommand{\clospan}[1][]{
	\ifempty{#1}
		\operatorname{\overline{span}}
	\else
		\operatorname{\overline{span}}\sset{#1}
	\fi
}
\newcommand{\Span}[1][]{
	\ifempty{#1}
		\misitex@Span
	\else
		\misitex@Span\sset{#1}
	\fi
}
\newcommand{\cloSpan}[1][]{
	\ifempty{#1}
		\operatorname{\overline{Span}}
	\else
		\operatorname{\overline{Span}}\sset{#1}
	\fi
}

%% Powerset
\ifthenelse{\equal{\misitex@psstyle}{mathbb}}{
	\newcommand{\powerset}[1]{\mathbb P\of{#1}}
}{
	\ifthenelse{\equal{\misitex@psstyle}{mathcal}}{
		\newcommand{\powerset}[1]{\mathcal P\of{#1}}
	}{
		\ifthenelse{\equal{\misitex@psstyle}{2}}{
			\newcommand{\powerset}[1]{2^{#1}}
		}{}
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Var- Letters			%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\let\obsoletephi\phi
\let\nvarphi\phi
\renewcommand{\phi}{\varphi}
\let\obsoleteepsilon\epsilon
\let\nvarepsilon\epsilon
\renewcommand{\epsilon}{\varepsilon}
\let\obsoletetheta\theta
\let\nvartheta\theta
\renewcommand{\theta}{\vartheta}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Functions				%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Restriction
\ifthenelse{\equal{\misitex@restrictionstyle}{harpoon}}
{
	\newcommand{\rest}{\mathord{\upharpoonright}}
	\newcommand{\restr}[2]{#1\rest_{#2}}
}
{
	\ifthenelse{\equal{\misitex@restrictionstyle}{bar}}
	{
		\newcommand\restr[2]{{%
				\left.\kern-\nulldelimiterspace%
				#1%
				\vphantom{\big|}%
				\right|_{#2}%
		}}
		\newcommand{\rest}{\restr{}{}}%
	}
	{}
}

%% Image, Kernel/Nullspace
\ifmisitex@uniqueimage
	\DeclareMathOperator{\Range}{\misitex@image}
	\DeclareMathOperator{\Rang}{\misitex@image}
	\DeclareMathOperator{\Ran}{\misitex@image}
	\DeclareMathOperator{\Rg}{\misitex@image}
	\DeclareMathOperator{\range}{\misitex@image}
	\DeclareMathOperator{\rang}{\misitex@image}
	\DeclareMathOperator{\ran}{\misitex@image}
	\DeclareMathOperator{\rg}{\misitex@image}
	\DeclareMathOperator{\bild}{\misitex@image}
	\DeclareMathOperator{\Bild}{\misitex@image}
	\DeclareMathOperator{\image}{\misitex@image}
	\DeclareMathOperator{\Image}{\misitex@image}
\else
	\DeclareMathOperator{\Range}{Ran}
	\DeclareMathOperator{\Rang}{Ran}
	\DeclareMathOperator{\Ran}{Ran}
	\DeclareMathOperator{\Rg}{Rg}
	\DeclareMathOperator{\range}{ran}
	\DeclareMathOperator{\rang}{ran}
	\DeclareMathOperator{\ran}{ran}
	\DeclareMathOperator{\rg}{rg}
	\DeclareMathOperator{\bild}{Bild}
	\DeclareMathOperator{\Bild}{Bild}
	\DeclareMathOperator{\image}{Image}
	\DeclareMathOperator{\Image}{Image}
\fi

%% argmin argmax
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\ifmisitex@uniquekernel
	\DeclareMathOperator{\Kern}{\misitex@kernel}
	\DeclareMathOperator{\Ker}{\misitex@kernel}
	\DeclareMathOperator{\kernel}{\misitex@kernel} % can't be \kern, that is LaTeX territory!
	%\DeclareMathOperator{\ker}{ker}%  already defined
	\DeclareMathOperator{\NS}{\misitex@kernel}
\else
	\DeclareMathOperator{\Kern}{Kern}
	\DeclareMathOperator{\Ker}{Ker}
	\DeclareMathOperator{\kernel}{kern} % can't be \kern, that is LaTeX territory!
	%\DeclareMathOperator{\ker}{ker}%  already defined
	\DeclareMathOperator{\NS}{N}
\fi

%% Graph of an Operator

\ifthenelse{\equal{\misitex@graphstyle}{operator}}
{
	\newrobustcmd*{\graph}{\operatorname{gra}}
	\newrobustcmd*{\Graph}{\operatorname{Gra}}
}
{
	\ifthenelse{\equal{\misitex@graphstyle}{gamma}}
	{
		\newrobustcmd*{\graph}{\upGamma}
		\newrobustcmd*{\Graph}{\upGamma}
	}{}
}


%% Indicator and characteristic function

\ifmisitex@uniqueind
	\newcommand{\ind}{\misitex@indcmd}
	\newcommand{\charac}{\misitex@indcmd}
\else
	\ifthenelse{\equal{\misitex@ind}{1}}
		{\newcommand{\ind}{\boldsymbol 1}}
		{
			\ifthenelse{\equal{\misitex@ind}{I}}
				{\newcommand{\ind}{\mathcal{I}}}
				{}
		}

	\ifthenelse{\equal{\misitex@charac}{1}}
		{\newcommand{\charac}{\mathds 1}}
		{
		\ifthenelse{\equal{\misitex@charac}{chi}}
			{\newcommand{\charac}{\chi}}
			{}
		}
\fi
\newcommand{\1}{\charac}


%% Misc
\DeclareMathOperator{\dom}{dom}
\DeclareMathOperator{\Dom}{Dom}
\newcommand{\pos}[1]{{#1}_+}
\newcommand{\negprt}[1]{{#1}_-}
\ifmisitex@uniquesupport
	\DeclareMathOperator{\spt}{\misitex@support}
	\DeclareMathOperator{\Spt}{\misitex@support}
	\DeclareMathOperator{\supp}{\misitex@support}
	\DeclareMathOperator{\Supp}{\misitex@support}
\else
	\DeclareMathOperator{\spt}{spt}
	\DeclareMathOperator{\supp}{supp}
	\DeclareMathOperator{\Spt}{Spt}
	\DeclareMathOperator{\Supp}{Supp}
\fi

%% Fourier & Convolution
%% Use the widecheck from mathabx
%% Setup the matha font (from mathabx.sty)
\DeclareFontFamily{U}{mathx}{\hyphenchar\font45}
\DeclareFontShape{U}{mathx}{m}{n}{
	<5> <6> <7> <8> <9> <10>
	<10.95> <12> <14.4> <17.28> <20.74> <24.88>
	mathx10
}{}
\DeclareSymbolFont{mathx}{U}{mathx}{m}{n}
\DeclareFontSubstitution{U}{mathx}{m}{n}

%% Define the widecheck accent that font (from mathabx.dcl)
\DeclareMathAccent{\widecheck}{0}{mathx}{"71}

\newrobustcmd{\fourier}[1]{\widehat{#1}}
\newrobustcmd{\invfourier}[1]{\widecheck{#1}}
\newcommand{\conv}{\ast}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Convergence			%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% General setup

\newlength{\convarrowlengthtop}
\newlength{\convarrowlengthbtm}

\makeatletter
\NewDocumentCommand{\misitex@stackarrow}{m m O{-2pt} m O{-7pt}}{%
	%% Get Arrow to use
	\let\misitex@stackconvarrow #1%
	%
	%% Get Dimension of text
	\settowidth{\convarrowlengthtop}{\scriptsize\ensuremath{#2\,}}%
	\settowidth{\convarrowlengthbtm}{\scriptsize\ensuremath{#4\,}}%
	%
	%% Typeset arrow
	\mathrel{%
		\stackunder[{#5}]{%
			\stackon[{#3}]{%
				\ensuremath{\misitex@stackconvarrow%
					[\hspace{\convarrowlengthbtm}]%
					{\hspace{\convarrowlengthtop}}%
			}}{\scriptsize\ensuremath{#2\,}}%
		}{\scriptsize\ensuremath{#4\,}}%
	}%
}

\NewDocumentCommand{\stackarrow}{m m O{-2pt} m O{-7pt}}{%
	\ifstrempty{#2}{%
		\ifstrempty{#4}{%
			#1{}%
		}{%
			\misitex@stackarrow{#1}{#2}[{#3}]{#4}[{#5}]%
		}%
	}{%
		\misitex@stackarrow{#1}{#2}[{#3}]{#4}[{#5}]%
	}%
}%

\NewDocumentCommand{\convarrow}{m O{-2pt} m O{-7pt}}{%
	\stackarrow{\xrightarrow}{#1}[{#2}]{#3}[{#4}]%
}%

\NewDocumentCommand{\convharpoonup}{m O{-2pt} m O{-7pt}}{%
	\stackarrow{\xrightharpoonup}{#1}[{#2}]{#3}[{#4}]%
}%

\NewDocumentCommand{\convharpoondown}{m O{-2pt} m O{-7pt}}{%
	\stackarrow{\xrightharpoondown}{#1}[{#2}]{#3}[{#4}]%
}%

%% Special arrows

\NewDocumentCommand\too{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\ifthenelse{\equal{#2}{top}}{%
			\convarrow{#1}{}%
		}{%
			\convarrow{}{#1}%
		}%
	}{\longrightarrow}
}
\NewDocumentCommand\tinf{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\too[{#1\tinf}][{#2}]
	}{
		\to\infty
	}
}
\NewDocumentCommand\tzero{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\too[{#1\tzero}][{#2}]
	}{
		\to0
	}
}
\ifthenelse{\equal{\misitex@weakconvstyle}{harpoon}}{%
	\ifthenelse{\equal{\misitex@convpos}{top}}{
		\let\misitex@wtoocmd\convharpoondown
		\let\misitex@wtooscmd\rightharpoondown
		\newcommand{\wsto}{\convharpoondown{}{*}[-8.5pt]}
	}{
		\let\misitex@wtoocmd\convharpoonup
		\let\misitex@wtooscmd\rightharpoonup
		\newcommand{\wsto}{\convharpoonup{*}[-3.5pt]{}}
	}
	\newcommand{\wto}{\misitex@wtooscmd{}{}}
	\NewDocumentCommand\wtoo{o O{\misitex@convpos}}{%
		\IfValueTF{#1}{%
			\ifthenelse{\equal{#2}{top}}{%
				\misitex@wtoocmd{#1}{}%
			}{%
				\misitex@wtoocmd{}{#1}%
			}
		}{\rightharpoonup}
	}
	\NewDocumentCommand\wstoo{O{} O{\misitex@convpos}}{%
		\ifstrempty{#1}%
			{\wsto}%
			{%
				\ifthenelse{\equal{#2}{top}}{%
					\convharpoondown{#1}{*}[-8.5pt]%
				}{%
					\convharpoonup{*}[-3.5pt]{#1}%
				}%
			}%
	}
}{\ifthenelse{\equal{\misitex@weakconvstyle}{w}}{%
		\newcommand{\wto}{\too[\mathrm{w}][\misitex@negconvpos]}
		\NewDocumentCommand\wtoo{O{} O{\misitex@convpos}}{%
			\ifstrempty{#1}
				{\too[\mathrm{w}][\misitex@negconvpos]}
				{
					\ifthenelse{\equal{#2}{top}}{
						\def\misitex@wtootop{#1}
						\def\misitex@wtoobtm{\mathrm{w}}
					}{
						\def\misitex@wtootop{\mathrm{w}}
						\def\misitex@wtoobtm{#1}
					}
					\convarrow{\misitex@wtootop}{\misitex@wtoobtm}
				}
		}
		\newcommand{\wsto}{\too[\mathrm{w}^*][\misitex@negconvpos]}
		\NewDocumentCommand\wstoo{O{} O{\misitex@convpos}}{%
			\ifstrempty{#1}
				{\too[\mathrm{w}^*][\misitex@negconvpos]}
				{
					\ifthenelse{\equal{#2}{top}}{
						\def\misitex@wtootop{#1}
						\def\misitex@wtoobtm{\mathrm{w}^*}
					}{
						\def\misitex@wtootop{\mathrm{w}^*}
						\def\misitex@wtoobtm{#1}
					}
					\convarrow{\misitex@wtootop}{\misitex@wtoobtm}
				}
		}
	}{}
}
\NewDocumentCommand\wtinf{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\wtoo[{#1\tinf}][{#2}]
	}{
		\wto\infty
	}
}
\NewDocumentCommand\wtzero{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\wtoo[{#1\zero}][{#2}]
	}{
		\wto0
	}
}
\NewDocumentCommand\wstinf{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\wstoo[{#1\tinf}][{#2}]
	}{
		\wsto\infty
	}
}
\NewDocumentCommand\wstzero{o O{\misitex@convpos}}{
	\IfValueTF{#1}{
		\wstoo[{#1\zero}][{#2}]
	}{
		\wsto0
	}
}

%% Strong limit

\newcommand*{\slim}{\operatorname*{s-lim}}

%% Weak limit

\newcommand{\wlim}{\operatorname*{w-lim}}

%% Weak* limit

\newcommand{\wslim}{\operatorname*{w^\ast\!-lim}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Functional Analysis	%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Embedding
\newrobustcmd{\embed}{\hookrightarrow}

%% Banach and Hilbert Spaces
\newcommand{\HR}[1]{\Space{#1}}
\newcommand{\HS}[1]{\Space{#1}}
\let\obsoleteH\H
\renewcommand{\H}{\HS{H}}
\newcommand{\BR}[1]{\Space{#1}}
\newcommand{\BS}[1]{\Space{#1}}

%% Dual space
\ifthenelse{\equal{\misitex@dualspacestyle}{ast}}{
	\newcommand{\dual}[1]{{#1}^*}
}{
	\ifthenelse{\equal{\misitex@dualspacestyle}{prime}}{
		\newcommand{\dual}[1]{{#1}'}
	}{}
}
\newcommand{\DS}[1]{\dual{#1}}
\newcommand{\DR}[1]{\dual{#1}}

%% Lebesgue L^p Spaces
\let\obsoleteL\L
\renewcommand{\L}[1]{
	\ifstrempty{#1}
	{\upSpace{L}}
	{{\upSpace{L}}^{\mspace{-4.5mu}{#1}}}
}
\newcommand{\Lloc}[1]{\L{#1}_{\mathrm{loc}}}
\newcommand{\Lp}{\L{p}}
\newcommand{\Lploc}{\Lloc{p}}
\newcommand{\Lq}{\L{q}}
\newcommand{\Lqloc}{\Lloc{q}}
\newcommand{\Linf}{\L{\infty}}

%% Sobolev Spaces

\NewDocumentCommand{\sobolev}{s m o d<>}{
	\IfNoValueTF{#3}{%
			\IfBooleanT{#1}{\mathring}{\upSpace{H}}^{#2}%
			\IfNoValueF{#4}{_{\text{#4}}}%
%			\IfBooleanT{#1}{_0}
		}{%
			\IfBooleanT{#1}{\mathring}{\upSpace{W}}^{#2,#3}%
			\IfNoValueF{#4}{_{\text{#4}}}%
%			\IfBooleanT{#1}{_0}
		}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% More special Spaces	%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Schwarz
\let\obsoleteS\S
\renewcommand{\SS}{\mathcal{S}}
\newcommand{\schwartz}{\SS}

%% Continuous and Test functions
\newcommand{\CC}{\Space{C}}
\newcommand{\CCc}{\Space{C}_{\mathrm{c}}}
\newcommand{\test}{\CC_{\mathrm{c}}^\infty}
\newcommand{\DD}{\mathcal{D}}

%% Quotient spaces
\let\nfrac\nicefrac
\newcommand{\QS}[2]{\nfrac{#1}{#2}}
\newcommand{\QR}[2]{\QS{#1}{#2}}

%% Linear Operators
\ifthenelse{\equal{\misitex@linopstyle}{frak}}
{
\newcommand*{\lin}[1][]{\ensuremath{\mathfrak{L}_{\mathrm{#1}}}}
\newcommand*{\Lin}[1][]{\ensuremath{\mathfrak{L}_{\mathrm{#1}}}}
\newcommand*{\linof}[2][]{\ensuremath{\mathfrak{L}_{\mathrm{#1}}\of{#2}}}
\newcommand*{\Linof}[2][]{\ensuremath{\mathfrak{L}_{\mathrm{#1}}\of{#2}}}
}
{
	\ifthenelse{\equal{\misitex@linopstyle}{text}}
	{
	\newcommand*{\lin}[1][]{\ensuremath{\op{lin}_{\mathrm{#1}}}}
	\newcommand*{\Lin}[1][]{\ensuremath{\op{lin}_{\mathrm{#1}}}}
	\newcommand*{\linof}[2][]{\ensuremath{\op{lin}_{\mathrm{#1}}{#2}}}
	\newcommand*{\Linof}[2][]{\ensuremath{\op{lin}_{\mathrm{#1}}{#2}}}
	}
	{
		\ifthenelse{\equal{\misitex@linopstyle}{cal}}
		{
		\newcommand*{\lin}[1][]{\ensuremath{\mathcal{L}_{\mathrm{#1}}}}
		\newcommand*{\Lin}[1][]{\ensuremath{\mathcal{L}_{\mathrm{#1}}}}
		\newcommand*{\linof}[2][]{\ensuremath{\mathcal{L}_{\mathrm{#1}}\of{#2}}}
		\newcommand*{\Linof}[2][]{\ensuremath{\mathcal{L}_{\mathrm{#1}}\of{#2}}}
		}
		{}
	}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Calculus				%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Derivative Setup
\ifthenelse{\equal{\misitex@derivativestyle}{fraction}}
{\NewDocumentCommand{\derivquotient}{o m m o}{\frac{{#2}\IfNoValueF{#1}{^{#1}}{\IfNoValueF{#4}{#4}}}{{#2}#3\IfNoValueF{#1}{^{#1}}}}}
{
	\ifthenelse{\equal{\misitex@derivativestyle}{subscript}}
	{\NewDocumentCommand{\derivquotient}{o m m o}{{#2}\IfNoValueF{#1}{^{#1}}_{#3}\IfNoValueF{#4}{{#4}}}}
	{}
}

%% Provides symbol for euclidean scalar product, in case one wants to use something other than \cdot
\newcommand{\skadot}{\cdot}
\newcommand{\scadot}{\cdot}

%% Spacial derivatives
\let\obsoletediv\div
\let\div\relax
\ifthenelse
	{\equal{\misitex@nablastyle}{operatornames}}
	{%
		\DeclareMathOperator{\div}{div}
		\DeclareMathOperator{\rot}{rot}
		\DeclareMathOperator{\grad}{grad}
		\DeclareMathOperator{\gradient}{grad}
	}
	{%
		\DeclareRobustCommand{\div}{\nabla \skadot}
		\DeclareRobustCommand{\rot}{\nabla \times}
		\DeclareRobustCommand{\grad}{\nabla}
		\DeclareRobustCommand{\gradient}{\nabla}
	}
\newrobustcmd{\laplace}{\upDelta}
\newrobustcmd{\subdiff}{\partial}
\newrobustcmd{\sdiff}{\partial}
\newrobustcmd{\del}{\nabla}

%% Total Derivative
\ifthenelse{\equal{\misitex@totalderivativestyle}{D}}
{
	\NewDocumentCommand{\tder}{o m}{\frechet\IfNoValueF{#1}{^{#1}}{#2}}
}
{
	\ifthenelse{\equal{\misitex@totalderivativestyle}{prime}}
	{
		\NewDocumentCommand{\tder}{o m}
			{
				\IfNoValueTF{#1}{{#2}'}
					{
						\IfStrEq{#1}{2}{{#2}''}
							{
								\IfStrEq{#1}{3}{{#2}'''}{{#2}^{\of{#1}}}
							}
					}
			}
	}
	{
		\ifthenelse{\equal{\misitex@totalderivativestyle}{d}}
		{
			\NewDocumentCommand{\tder}{o m}{\d\IfNoValueF{#1}{^{#1}}{#2}}
		}
		{
				\NewDocumentCommand{\tder}{o m}{\pard\IfNoValueF{#1}{^{#1}}{#2}}
		}
	}
}


%% Subscript derivatives
\ifthenelse{\equal{\misitex@subscriptderivativestyle}{comma}}
{
	\NewDocumentCommand{\misitex@dd}{m m m o}{
		\IfValueTF{#4}%
		{{#1}_{#4\derivsep{#3}}}%
		{%
			\IfValueTF{#2}%
			{{#1}_{#2\derivsep{#3}}}%
			{{#1}_{\derivsep{#3}}}%
		}%
	}
}
{
	\ifthenelse{\equal{\misitex@subscriptderivativestyle}{brackets}}
	{
		\NewDocumentCommand{\misitex@dd}{m m m o}{
			\IfValueTF{#4}%
			{{\left({#1}_{#4}\right)}_{#3}}%
			{%
				\IfValueTF{#2}%
				{{\left({#1}_{#2}\right)}_{#3}}%
				{{#1}_{#3}}%
			}%
		}
	}{}
}
\NewDocumentCommand{\dd}{o > { \SplitArgument { 1 } { _ } } m m}{
	\IfNoValueTF{#1}
		{\misitex@dd #2 {#3}}
		{\misitex@dd #2 {#3} [{#1}]}
}

%% uppercase D for (total) Frechet derivatives
\newrobustcmd{\frechet}{\ensuremath{\mathop{}\!\mathrm{D}}}

%% Save Dot unter command
\let\dotunter\d
\let\obsoleted\d

%% General setup for "ordinary" derivs
\renewcommand*{\d}{\ensuremath{\mathop{}\!\mathrm{d}}}
\ifcsmacro{dq}
	{
		\let\obsoletedq\dq
	\RenewDocumentCommand{\dq}{o m o}{\derivquotient[#1]{\d}{#2}[#3]}
	}
	{\NewDocumentCommand{\dq}{o m o}{\derivquotient[#1]{\d}{#2}[#3]}}
\newcommand*{\sdq}[1]{\dq[2]{#1}}%

\newcommand*{\pard}{\ensuremath{\mathop{}\!\partial}}

\NewDocumentCommand{\pardq}{o m o}{\derivquotient[#1]{\pard}{#2}[#3]}
\NewDocumentCommand{\spardq}{m o}{\pardq[2]{#1}[#2]}%

%% Derivatives as operators regardless of option:
\NewDocumentCommand{\der}{o m o}{\d\IfNoValueF{#1}{^{#1}}_{#2}\IfNoValueF{#3}{#3}}
\NewDocumentCommand{\pder}{o m o}{\pard\IfNoValueF{#1}{^{#1}}_{#2}\IfNoValueF{#3}{#3}}

%% Special multidim. derivatives
\newcommand*{\dxy}{\d (x,y)}
\newcommand*{\dxyz}{\d (x,y,z)}

\newcommand*{\dqxy}[1][]{\dq[{#1}]{(x,y)}}
\newcommand*{\dqxyz}[1][]{\dq[{#1}]{(x,y,z)}}

\newcommand*{\sdqxy}{\dq[2]{(x,y)}}
\newcommand*{\sdqxyz}{\dq[2]{(x,y,z)}}

\newcommand*{\pardxy}{\pard (x,y)}
\newcommand*{\pardxyz}{\pard (x,y,z)}

\newcommand*{\pardqxy}[1][]{\pardq[{#1}]{(x,y)}}
\newcommand*{\pardqxyz}[1][]{\pardq[{#1}]{(x,y,z)}}

\newcommand*{\spardqxy}{\pardq[2]{(x,y)}}
\newcommand*{\spardqxyz}{\pardq[2]{(x,y,z)}}

%% Define commands in a loop
\ifmisitex@allderivatives
	\foreach \x in {a,...,z}{
		%% Helpers for optional arguments
		\global\expandafter\edef\csname misitex@dq\x\endcsname[##1]{
			\noexpand\dq[{##1}]{\x}
		}
		\global\expandafter\edef\csname misitex@pardq\x\endcsname[##1]{
			\noexpand\pardq[{##1}]{\x}
		}
		%% Don't orverwrite existing commands
		\ifcsname d\x\endcsname\else
			\global\expandafter\edef\csname d\x\endcsname{\noexpand\d \x}
			\global\expandafter\def\csname misitex@d\x @ismacro\endcsname{}
		\fi
		\ifcsname pard\x\endcsname\else
			\global\expandafter\edef\csname pard\x\endcsname{\noexpand\pard \x}
			\global\expandafter\def\csname misitex@d\x @ismacro\endcsname{}
		\fi
		%% \(par)d...
		\global\expandafter\edef\csname dq\x\endcsname{%
			\noexpand\@ifnextchar[%
			{\expandafter\noexpand\csname misitex@dq\x\endcsname}%
			{\expandafter\noexpand\csname misitex@dq\x\endcsname[]}%
		}
		\global\expandafter\edef\csname pardq\x\endcsname{%
			\noexpand\@ifnextchar[%
			{\expandafter\noexpand\csname misitex@pardq\x\endcsname}%
			{\expandafter\noexpand\csname misitex@pardq\x\endcsname[]}%
		}
		%% \s(par)d...
		\global\expandafter\edef\csname sdq\x\endcsname{%
			\noexpand\dq[2]{\x}
		}
		\global\expandafter\edef\csname spardq\x\endcsname{%
			\noexpand\pardq[2]{\x}
		}
	}

	%% Repeat for greek letters
	\foreach \x in {alpha,beta,gamma,delta,epsilon,zeta,eta,theta,iota,kappa,	lambda,mu,nu,xi,pi,rho,sigma,tau,phi,chi,psi,omega}{
		%% Helpers for optional arguments
		\global\expandafter\edef\csname misitex@dq\x\endcsname[##1]{
			\noexpand\dq[{##1}]{\csname\x\endcsname}
		}
		\global\expandafter\edef\csname misitex@pardq\x\endcsname[##1]{
			\noexpand\pardq[{##1}]{\csname\x\endcsname}
		}
		%% Don't orverwrite existing commands
		\ifcsname d\x\endcsname\else
			\global\expandafter\edef\csname d\x\endcsname{%
				\noexpand\d \expandafter\noexpand\csname\x\endcsname}
			\global\expandafter\def\csname misitex@d\x @ismacro\endcsname{}
		\fi
		\ifcsname pard\x\endcsname\else
			\global\expandafter\edef\csname pard\x\endcsname{%
				\noexpand\pard \expandafter\noexpand\csname\x\endcsname}
			\global\expandafter\def\csname misitex@pard\x @ismacro\endcsname{}
		\fi
		%% \(par)d...
		\global\expandafter\edef\csname dq\x\endcsname{%
			\noexpand\@ifnextchar[%
			{\expandafter\noexpand\csname misitex@dq\x\endcsname}%
			{\expandafter\noexpand\csname misitex@dq\x\endcsname[]}%
		}
		\global\expandafter\edef\csname pardq\x\endcsname{%
			\noexpand\@ifnextchar[%
			{\expandafter\noexpand\csname misitex@pardq\x\endcsname}%
			{\expandafter\noexpand\csname misitex@pardq\x\endcsname[]}%
		}
		%% \s(par)d...
		\global\expandafter\edef\csname sdq\x\endcsname{%
			\noexpand\dq[2]{\expandafter\noexpand\csname\x\endcsname}
		}
		\global\expandafter\edef\csname spardq\x\endcsname{%
			\noexpand\pardq[2]{\expandafter\noexpand\csname\x\endcsname}
		}
	}
\fi